# Використовуємо офіційний образ Python
FROM python:3.11-slim

WORKDIR /code

COPY . .

ENV TZ=Europe/Kiev

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONFAULTHANDLER 1 
ENV PIP_NO_CACHE_DIR off
ENV PIP_DISABLE_PIP_VERSION_CHECK on
ENV PIP_DEFAULT_TIMEOUT 100

ENV FASTAPI_ENV production

RUN pip install --no-cache-dir --upgrade -r requirements.txt

EXPOSE 8000

# CMD ["uvicorn", "app.main:app", "--reload", "--host", "0.0.0.0", "--port", "8080"] # "--reload" тільки для розробки, жре ресурси
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080"]

# # Етап 1: Збірка додатку
# # FROM python:3.11-alpine AS base
# FROM python:3.11-slim AS base

# FROM base as builder
# # FROM python:3.11-slim as builder

# RUN mkdir /install
# WORKDIR /install

# WORKDIR /code

# COPY requirements.txt .

# # ENV PIP_DEFAULT_TIMEOUT=100 \
# #     # Allow statements and log messages to immediately appear
# #     PYTHONUNBUFFERED=1 \
# #     # disable a pip version check to reduce run-time & log-spam
# #     PIP_DISABLE_PIP_VERSION_CHECK=1 \
# #     # cache is useless in docker image, so disable to reduce image size
# #     PIP_NO_CACHE_DIR=1

# # PYTHONFAULTHANDLER=1 - Display trace if a sefault occurs.
# # PYTHONUNBUFFERED=1 - Allow statements and log messages to immediately appear in the Knative logs
# # PIP_NO_CACHE_DIR=off - Disable pip cache for smaller Docker images.
# # PIP_DISABLE_PIP_VERSION_CHECK=on - Ignore pip new version warning.
# # PIP_DEFAULT_TIMEOUT=100 - Give pip longer than the 15 second timeout.
# # ENV DOCKER_ENV=${DOCKER_ENV} \
# #   PYTHONFAULTHANDLER=1 \
# #   PYTHONUNBUFFERED=1 \
# #   PIP_NO_CACHE_DIR=off \
# #   PIP_DISABLE_PIP_VERSION_CHECK=on \
# #   PIP_DEFAULT_TIMEOUT=100

# # RUN pip install --no-cache-dir -r requirements.txt
# # RUN pip install --upgrade pip
# # RUN apk update && apk add -y libopenblas-dev

# RUN python -m venv /opt/venv

# ENV PATH="/opt/venv/bin:${PATH}"

# # RUN pip install -r requirements.txt -t /install
# RUN pip install -r requirements.txt

# COPY . .

# # ENV TZ=Europe/Kiev
# # ENV PYTHONDONTWRITEBYTECODE 1
# # ENV PYTHONUNBUFFERED 1

# # Етап 2: Запуск додатку
# FROM base

# WORKDIR /code

# COPY --from=builder /opt/venv /opt/venv
# # COPY --from=builder /root/.local /root/.local
# ENV PATH="/opt/venv/bin:${PATH}"
# # ENV PATH=/root/.local/bin:$PATH
# # COPY --from=builder /install /usr/local
# COPY --from=builder /code /code

# ENV TZ=Europe/Kiev
# ENV PYTHONDONTWRITEBYTECODE 1
# ENV PYTHONUNBUFFERED 1

# EXPOSE 8000

# # CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
# ENTRYPOINT uvicorn app.main:app --reload --host 0.0.0.0 --port 8000




# FROM python:3-alpine as builder

# ENV PATH="/opt/venv/bin:${PATH}"

# COPY requirements.txt  /code/requirements.txt

# RUN \
#   apk update && \
#   apk add build-base && \
#   python -m venv /opt/venv && \
#   pip install --upgrade pip && \
#   pip install -r /code/requirements.txt

# FROM python:3-alpine

# # ARG UID=1005
# # ARG USERNAME=flask

# COPY .  /code
# COPY --from=builder /opt/venv /opt/venv

# ENV PATH="/opt/venv/bin:${PATH}"

# # RUN \
# #   apk update && \
# #   adduser -g ${USERNAME} ${USERNAME} --disabled-password --uid ${UID} && \
# #   chown -R ${USERNAME} /u01

# EXPOSE 8080
# WORKDIR /code

# # USER ${USERNAME}

# # ENTRYPOINT [ "gunicorn", "--bind", "0.0.0.0:8080", "wsgi:app" ]
# CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]