from pathlib import Path
from sqlmodel import SQLModel, Session, create_engine


path_db = Path('./db')

if not path_db.exists():
    path_db.mkdir(parents=True, exist_ok=True)

sqlite_file_name = './db/ip_locations.db'
sqlite_url = f'sqlite:///{sqlite_file_name}'

connect_args = {'check_same_thread': False}
engine = create_engine(sqlite_url, echo=False, connect_args=connect_args)

def create_db_and_tables():
    SQLModel.metadata.create_all(engine)

def get_session():
    with Session(engine) as session:
        yield session
