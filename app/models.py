from pydantic import BaseModel, Field
from typing import Optional

class IPLocation(BaseModel):
    id: Optional[int] = Field(default=None, primary_key=True)
    start_ip: Optional[str] = None
    end_ip: Optional[str] = None
    start_ip_num: Optional[int] = Field(..., index=True)
    end_ip_num: Optional[int] = Field(..., index=True)
    country: Optional[str] = None
    city: Optional[str] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None

    class Config:
        orm_mode = True
