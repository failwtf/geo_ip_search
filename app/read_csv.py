# import pandas as pd

# df = pd.read_csv("db/dbip-city-ipv4.csv", engine="pyarrow")

# print(type(df))

# from pyarrow import csv
# fn = 'db/geolite2-city-ipv4-num.csv'
# table = csv.read_csv(fn)
# print(type(table))
# print(len(table))

# df = table.to_pandas()

# print(df.head())

# print(table)



from pyarrow import csv
import ipaddress
import pandas as pd
import time

fn = 'db/geolite2-city-ipv4-num.csv'
table = csv.read_csv(fn)

# Перетворення таблиці PyArrow у DataFrame Pandas
df = table.to_pandas()

def find_location_by_ip(ip_address):
    ip_numeric = int(ipaddress.IPv4Address(ip_address))
    mask = (df['ip_range_start'] <= ip_numeric) & (df['ip_range_end'] >= ip_numeric)
    
    start_time = time.time()  # Запам'ятати час початку пошуку
    matching_rows = df[mask]
    end_time = time.time()  # Запам'ятати час завершення пошуку

    if not matching_rows.empty:
        

        row = matching_rows.iloc[0]
        location = {
            'ip_range_start': row['ip_range_start'],
            'ip_range_end': row['ip_range_end'],
            'country_code': row['country_code'],
            'state1': row['state1'],
            'state2': row['state2'],
            'city': row['city'],
            'postcode': row['postcode'],
            'latitude': row['latitude'],
            'longitude': row['longitude'],
            'timezone': row['timezone']
        }

        
        execution_time = round(end_time - start_time, 3)  # Обчислити час виконання

        location['execution_time'] = execution_time
        return location
    else:
        return None

# Приклад виклику функції пошуку
ip_to_search = "79.110.132.30"
result = find_location_by_ip(ip_to_search)

if result:
    print("Місцезнаходження знайдено:")
    print(result)
    print("Час виконання пошуку:", result['execution_time'], "секунд")
else:
    print(f"Місцезнаходження для IP {ip_to_search} не знайдено.")




# from pyarrow import csv
# import pandas as pd

# fn = 'db/geolite2-city-ipv4-num.csv'
# table = csv.read_csv(fn)

# df = table.to_pandas()

# # Вивести назви колонок
# column_names = df.columns.tolist()
# print(column_names)

# print(df.head())


# from pyarrow import csv
# import pandas as pd

# fn = 'db/geolite2-city-ipv4-num.csv'

# # Читання вмісту файлу у список рядків
# with open(fn, 'r') as file:
#     lines = file.readlines()

# # Вставка нового рядка з назвами колонок
# column_names = ['ip_range_start', 'ip_range_end', 'country_code', 'state1', 'state2', 'city', 'postcode', 'latitude', 'longitude', 'timezone']
# lines.insert(0, ','.join(column_names) + '\n')

# # Запис зміненого списку рядків назад у файл
# with open(fn, 'w') as file:
#     file.writelines(lines)

# # Зчитування CSV-файлу за допомогою PyArrow
# table = csv.read_csv(fn)
# df = table.to_pandas()

# # Вивести перші кілька рядків DataFrame
# print(df.head())

