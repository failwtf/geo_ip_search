import os
import gzip
import time
import requests
import ipaddress
from pyarrow import csv
import ipaddress


# URL для завантаження
url = "https://github.com/sapics/ip-location-db/raw/main/geolite2-city/geolite2-city-ipv4-num.csv.gz"

# Шлях для збереження завантаженого файлу
download_path = "db/tmp/geolite2-city-ipv4-num.csv.gz"

# Шлях для розпакування стиснутого файлу
uncompressed_path = "db/geolite2-city-ipv4-num.csv"

# Шлях для збереження локальної версії
version_path = "db/version.txt"

def get_global_version():
    # URL для читання версії
    url = "https://github.com/sapics/ip-location-db/raw/main/geolite2-city/package.json"

    # Отримання JSON з URL
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        version = data.get("version")
        print(f"Глобальна версія: {version}")
        return version
    else:
        print("Не вдалося отримати JSON з URL.")
        return None
    
    
def get_local_version():
    try:
        with open(version_path, "r") as f:
            version = f.read().strip()
            print(f"Локальна версія: {version}")
            return version
    except FileNotFoundError:
        print("Локальну версію не знайдено.")
        return None
    

def update_local_version(version):
    if version:
        with open(version_path, "w") as f:
            f.write(version)
        print(f"Локальна версія: {version} збережена у файлі 'version.txt'")
    

def download_location(version):
    
    # Завантаження файлу
    response = requests.get(url)
    if response.status_code == 200:
        with open(download_path, "wb") as f:
            f.write(response.content)
        print("Файл завантажено успішно.")

        # Розпакування стиснутого файлу
        with gzip.open(download_path, "rb") as f_in, open(uncompressed_path, "wb") as f_out:
            f_out.write(f_in.read())
        print("Файл розпаковано успішно.")

        # Видалення стиснутого файлу
        os.remove(download_path)
        print("Стиснутий файл видалено.")

        # Читання вмісту файлу у список рядків
        with open(uncompressed_path, 'r') as file:
            lines = file.readlines()

        # Вставка нового рядка з назвами колонок
        column_names = ['ip_range_start', 'ip_range_end', 'country_code', 'state1', 'state2', 'city', 'postcode', 'latitude', 'longitude', 'timezone']
        lines.insert(0, ','.join(column_names) + '\n')

        # Запис зміненого списку рядків назад у файл
        with open(uncompressed_path, 'w') as file:
            file.writelines(lines)
        print("Вставка нового рядка з назвами колонок.")
        
        update_local_version(version)

    else:
        print("Не вдалося завантажити файл.")


def update_location():

    global_version = get_global_version()
    if global_version:
        local_version = get_local_version()
        if local_version:
            if global_version != local_version:
                download_location(global_version)
        else:
            download_location(global_version)
                    

    # Перетворення таблиці PyArrow у DataFrame Pandas
    global df

    table   = csv.read_csv(uncompressed_path)
    df      = table.to_pandas()


def find_location_by_ip(ip_address):

    start_time = time.time()  # Запам'ятати час початку пошуку

    ip_numeric      = int(ipaddress.IPv4Address(ip_address))
    mask            = (df['ip_range_start'] <= ip_numeric) & (df['ip_range_end'] >= ip_numeric)   
    matching_rows   = df[mask]

    if not matching_rows.empty:
        
        row = matching_rows.iloc[0]

        location = {}
        
        for column_name in df.columns:
            location[column_name] = str(row[column_name])
        
        end_time = time.time()  # Запам'ятати час завершення пошуку
        
        execution_time = round(end_time - start_time, 3)  # Обчислити час виконання
        location['execution_time'] = execution_time

        return location

    else:
        return None
