import requests
import gzip
import os

# URL для завантаження
# url = "https://github.com/sapics/ip-location-db/raw/main/dbip-city/dbip-city-ipv4.csv.gz"
# url = "https://github.com/sapics/ip-location-db/raw/main/dbip-city/dbip-city-ipv4-num.csv.gz"
url = "https://github.com/sapics/ip-location-db/raw/main/geolite2-city/geolite2-city-ipv4-num.csv.gz"

# Шлях для збереження завантаженого файлу
# download_path = "db/tmp/dbip-city-ipv4.csv.gz"
# download_path = "db/tmp/dbip-city-ipv4-num.csv.gz"
download_path = "db/tmp/geolite2-city-ipv4-num.csv.gz"

# Завантаження файлу
response = requests.get(url)
if response.status_code == 200:
    with open(download_path, "wb") as f:
        f.write(response.content)
    print("Файл завантажено успішно.")
else:
    print("Не вдалося завантажити файл.")

# Розпакування стиснутого файлу
# uncompressed_path = "db/dbip-city-ipv4.csv"
# uncompressed_path = "db/dbip-city-ipv4-num.csv"
uncompressed_path = "db/geolite2-city-ipv4-num.csv"

with gzip.open(download_path, "rb") as f_in, open(uncompressed_path, "wb") as f_out:
    f_out.write(f_in.read())

print("Файл розпаковано успішно.")

# Опціонально: Видалення стиснутого файлу
os.remove(download_path)
print("Стиснутий файл видалено.")

fn = "db/geolite2-city-ipv4-num.csv"

# Читання вмісту файлу у список рядків
with open(fn, 'r') as file:
    lines = file.readlines()

# Вставка нового рядка з назвами колонок
column_names = ['ip_range_start', 'ip_range_end', 'country_code', 'state1', 'state2', 'city', 'postcode', 'latitude', 'longitude', 'timezone']
lines.insert(0, ','.join(column_names) + '\n')

# Запис зміненого списку рядків назад у файл
with open(fn, 'w') as file:
    file.writelines(lines)

print("Вставка нового рядка з назвами колонок.")