# import sqlite3
# import csv
# import ipaddress
# import time


# # Функція для створення та заповнення бази даних
# def create_and_populate_database(db_file, csv_file):
#     conn = sqlite3.connect(db_file)
#     cursor = conn.cursor()

#     cursor.execute('''
#         CREATE TABLE IF NOT EXISTS ip_locations (
#             id INTEGER PRIMARY KEY,
#             start_ip TEXT,
#             end_ip TEXT,
#             start_ip_num INTEGER,
#             end_ip_num INTEGER,
#             country TEXT,
#             city TEXT,
#             latitude REAL,
#             longitude REAL
#         )
#     ''')
#     cursor.execute('CREATE INDEX IF NOT EXISTS idx_start_ip_num ON ip_locations (start_ip_num)')
#     cursor.execute('CREATE INDEX IF NOT EXISTS idx_end_ip_num ON ip_locations (end_ip_num)')

#     conn.commit()

#     with open(csv_file, newline='', encoding='utf-8') as csvfile:
#         csvreader = csv.reader(csvfile)
#         for row in csvreader:
#             start_ip_num = int(ipaddress.IPv4Address(row[0]))
#             end_ip_num = int(ipaddress.IPv4Address(row[1]))
#             # start_ip_num = int(row[0])
#             # end_ip_num = int(row[1])

#             cursor.execute('''
#                 INSERT INTO ip_locations (start_ip, end_ip, start_ip_num, end_ip_num, country, city, latitude, longitude)
#                 VALUES (?, ?, ?, ?, ?, ?, ?, ?)
#             ''', (row[0], row[1], start_ip_num, end_ip_num, row[2], row[5], float(row[7]), float(row[8])))

#         conn.commit()

#     conn.close()

# # # Функція для пошуку місцезнаходження за IP
# # def find_location(ip_address):
# #     with sqlite3.connect('db/ip_locations.db') as conn:
# #         cursor = conn.cursor()

# #         ip_numeric = int(ipaddress.IPv4Address(ip_address))

# #         query = "SELECT rowid, country, city, latitude, longitude FROM ip_locations WHERE ? BETWEEN start_ip_num AND end_ip_num LIMIT 1"

# #         start_time = time.time()  # Запам'ятати час початку пошуку

# #         cursor.execute(query, (ip_numeric,))
# #         result = cursor.fetchone()

# #         end_time = time.time()  # Запам'ятати час завершення пошуку

# #     if result:
# #         columns = ['rowid', 'country', 'city', 'latitude', 'longitude']
# #         location_data = dict(zip(columns, result))
# #         execution_time = round(end_time - start_time, 3)  # Округлити
# #         location_data['execution_time'] = execution_time
# #         return location_data
# #     else:
# #         return None

# # Виклик функції для створення та заповнення бази даних
# create_and_populate_database('db/ip_locations.db', 'db/dbip-city-ipv4.csv')

# # Виклик функції для пошуку та виведення результату
# # target_ip = "79.110.132.30"
# # location = find_location(target_ip)

# # if location:
# #     print("Знайдено місцезнаходження для IP", target_ip)
# #     print("Номер рядка:", location['rowid'])
# #     print("Країна:", location['country'])
# #     print("Місто:", location['city'])
# #     print("Широта:", location['latitude'])
# #     print("Довгота:", location['longitude'])
# #     print("Час виконання пошуку:", location['execution_time'], "секунд")
# # else:
# #     print("Місцезнаходження для IP", target_ip, "не знайдено")


import csv
import ipaddress

# Створити словник для зберігання даних
ip_location_data = {}

# Завантажити дані з CSV-файлу
with open('db/dbip-city-ipv4.csv', newline='', encoding='utf-8') as csvfile:
    csvreader = csv.reader(csvfile)
    for row in csvreader:
        start_ip_num = int(ipaddress.IPv4Address(row[0]))
        end_ip_num = int(ipaddress.IPv4Address(row[1]))
        ip_location_data[(start_ip_num, end_ip_num)] = {
            'start_ip': row[0],
            'end_ip': row[1],
            'country': row[2],
            'city': row[5],
            'latitude': float(row[7]),
            'longitude': float(row[8])
        }

# Функція для пошуку місцезнаходження за IP
def find_location(ip_address):
    ip_numeric = int(ipaddress.IPv4Address(ip_address))
    for ip_range, location in ip_location_data.items():
        start_ip_num, end_ip_num = ip_range
        if start_ip_num <= ip_numeric <= end_ip_num:
            return location
    return None

# Виклик функції для пошуку та виведення результату
target_ip = "79.110.132.30"
location = find_location(target_ip)

if location:
    print("Знайдено місцезнаходження для IP", target_ip)
    print("Країна:", location['country'])
    print("Місто:", location['city'])
    print("Широта:", location['latitude'])
    print("Довгота:", location['longitude'])
else:
    print("Місцезнаходження для IP", target_ip, "не знайдено")
