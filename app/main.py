from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from app.crud import update_location, find_location_by_ip


app = FastAPI()


@app.on_event("startup")
async def startup_event():
    update_location()

@app.get("/")
async def read_root():
    return {"message": "Welcome to IP Location API"}

@app.get("/ping")
async def read_ping():
    return {"message": "pong!"}

@app.get("/ip_location")
async def get_ip_location(ip: str):
    
    location = find_location_by_ip(ip)  
    if location:
        return location
    else:
        raise HTTPException(status_code=404, detail=f"Location for IP {ip} not found")

